# TP3 : Progressons vers le réseau d'infrastructure
## I. (mini)Architecture réseau
- Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme : 

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.0.128`        | `255.255.255.192` | `62`                           | `10.3.0.190`         | `10.3.0.191`                                                                                   |
| `server1`     | `10.3.0.0`        | `255.255.255.128` | `126`                           | `10.3.0.126`         | `10.3.0.127`                                                                                   |
| `server2`     | `10.3.0.192`        |`255.255.255.240` | `14`                           | `10.3.0.206`         | `10.3.0.207`    

- Vous remplirez aussi au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, le 🗃️ tableau d'adressage 🗃️ suivant : 

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190`         | `10.3.0.126`         | `10.3.0.207`         | Carte NAT             |
| `dhcp.client1.tp3`        | `10.3.0.189`                  | ...                  | ...                  | `10.3.0.190`      |  |
| `marcel.client1.tp3`        | `IP Dynamique`                  | ...                  | ...                  | `10.3.0.190`      |  |
| `dns1.server1.tp3`        | `10.3.0.125`                  | ...                  | ...                  | `10.3.0.126`      | 
| `johnny.client1.tp33`        | `IP Dynamique`                  | ...                  | ...                  | `10.3.0.126`      | 
| `web1.server2.tp3` | ...  | ... | `10.3.0.194` | `10.3.0.207` |
| `nfs1.server2.tp3` |  ... | ... | `10.3.0.195` | `10.3.0.207`

## II. Services d'infra
### 1. Serveur DHCP
- Mettre en place une machine qui fera office de serveur DHCP
```
[kalop@dhcp ~]$ sudo cat  /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.3.0.128 netmask 255.255.255.192 {
  range 10.3.0.130 10.3.0.188;
  option routers 10.3.0.190;
  option subnet-mask 255.255.255.192;
  option domain-name-servers 1.1.1.1;
}
[kalop@dhcp ~]$
```
- Mettre en place un client dans le réseau client1
- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP
```
[kalop@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=25.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=20.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=37.1 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=26.3 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 20.381/27.265/37.086/6.100 ms
[kalop@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 21285
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               2318    IN      A       92.243.16.143

;; Query time: 10 msec
;; SERVER: 10.33.10.148#53(10.33.10.148)
;; WHEN: Thu Sep 30 10:09:30 CEST 2021
;; MSG SIZE  rcvd: 53

[kalop@marcel ~]$
```
- à l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau
```
[kalop@marcel ~]$ traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  1.033 ms  1.061 ms  1.050 ms
 2  10.0.2.2 (10.0.2.2)  0.986 ms  0.968 ms  0.954 ms
 3  10.0.2.2 (10.0.2.2)  5.806 ms  28.752 ms  28.726 ms
[kalop@marcel ~]$
```
### 2. Serveur DNS
#### A. Our own DNS server

- Nom de dns1 ainsi que son ip dans le réseau server1 : 
```
[kalop@dns1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:49:89:d9 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86358sec preferred_lft 86358sec
    inet6 fe80::a00:27ff:fe49:89d9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:15:0e:77 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.125/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe15:e77/64 scope link
       valid_lft forever preferred_lft forever
[kalop@dns1 ~]$
```
- La config de la carte réseau

```
[kalop@dns1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8

BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
NETMASK=255.255.255.128
IPADDR=10.3.0.125
GATEWAY=10.3.0.126
DNS1=1.1.1.1
```
- Install de bind 
```
[kalop@dns1 ~]$dnf install -y bind bind-utils
```

- Fichier named.conf
```
[kalop@dns1 ~]$ sudo cat /etc/named.conf
options {
        listen-on port 53 { any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";

        recursion yes;
        allow-query     { any; };

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "server1.tp3" {
        type master;
        file "/etc/bind/zones/server1.tp3.forward";
        allow-query     { any; };
};

zone "server2.tp3" {
        type master;
        file "/etc/bind/zones/server2.tp3.forward";
        allow-query     { any; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

- Fichier server1.tp3.forward
```
[kalop@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
$TTL    86400
@       IN      SOA     dns1.server1.tp3. root.server1.tp3. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Minimum

@       IN      NS      dns1.server1.tp3.
dns1    IN      A       10.3.0.125

router  IN      A       10.3.0.126
```

- Fichier server2.tp3.forward
```
[kalop@dns1 ~]$ sudo cat /var/named/server2.tp3.forward
$TTL    86400
@       IN      SOA     dns1.server2.tp3. root.server2.tp3. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Minimum;

@       IN      NS      dns1.server2.tp3.
dns1    IN      A       10.3.0.125

router  IN      A       10.3.0.207
```

- On start et enabled le service named Et on ouvre le port nécéssaire sur le firewall
```
[kalop@dns1 ~]$ systemctl enable --now named
[kalop@dns1 ~]$ firewall-cmd --add-service=dns --permanent;firewall-cmd --reload 
```

- Tester le DNS depuis marcel.client1.tp3


```
[kalop@marcel ~]$ dig google.com @10.3.0.125

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com @10.3.0.125
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 14730
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 14c38858a4b7d05dbb622f26616f24d3fb426fb98b13fe58 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       216.58.213.174

;; AUTHORITY SECTION:
google.com.             172799  IN      NS      ns3.google.com.
google.com.             172799  IN      NS      ns1.google.com.
google.com.             172799  IN      NS      ns2.google.com.
google.com.             172799  IN      NS      ns4.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172799  IN      A       216.239.34.10
ns1.google.com.         172799  IN      A       216.239.32.10
ns3.google.com.         172799  IN      A       216.239.36.10
ns4.google.com.         172799  IN      A       216.239.38.10
ns2.google.com.         172799  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172799  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172799  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172799  IN      AAAA    2001:4860:4802:38::a

;; Query time: 1275 msec
;; SERVER: 10.3.0.125#53(10.3.0.125)
;; WHEN: Tue Oct 19 22:04:35 CEST 2021
;; MSG SIZE  rcvd: 331

```

- On teste sur la zone forward
```
    [kalop@marcel ~]$ dig dns1.server1.tp3 @10.3.0.125

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3 @10.3.0.125
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4634
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 7b945f67f3e33409b333759e616f2500db9b3807d948b90f (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.125

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 0 msec
;; SERVER: 10.3.0.125#53(10.3.0.125)
;; WHEN: Tue Oct 19 22:05:20 CEST 2021
;; MSG SIZE  rcvd: 103



```


## 3. Get deeper
### A. DNS forwarder
- Affiner la configuration du DNS
```
[...]
        recursion yes;
[...]
```

- Marcel peut résoudre des noms publics comme google.com en utilisant mon serveur DNS 
```
[kalop@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 15834
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             178     IN      A       216.58.215.46

;; Query time: 67 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Tue Oct 19 22:06:34 CEST 2021
;; MSG SIZE  rcvd: 55


```


## B. On revient sur la conf du DHCP

- Le DHCP donne désormais l'adresse de mon serveur DNS aux clients
```
[kalop@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 900;
[...]
  option domain-name-servers 10.3.0.125;
}

```


- qui récupére ses informations en grace au dhcp
```
[kalop@johnny ~]$ cat /etc/sysc
sysconfig/   sysctl.conf  sysctl.d/
[kalop@johnny ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
```

## III. Services métier
- Setup d'une nouvelle machine, qui sera un serveur Web
 
- Serveur NGINX

```
[kalop@web1 ~]$ sudo dnf install nginx
[...]
[kalop@web1 ~]$ sudo systemctl start nginx
[kalop@web1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-19 22:28:46 CEST; 6s ago
  Process: 37735 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 37734 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 37732 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 37737 (nginx)
    Tasks: 2 (limit: 4946)
   Memory: 3.7M
   CGroup: /system.slice/nginx.service
           ├─37737 nginx: master process /usr/sbin/nginx
           └─37738 nginx: worker process

Oct 19 22:28:46 web1.server2.tp3 systemd[1]: Starting The nginx HTTP and reverse proxy server...
Oct 19 22:28:46 web1.server2.tp3 nginx[37734]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Oct 19 22:28:46 web1.server2.tp3 nginx[37734]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Oct 19 22:28:46 web1.server2.tp3 systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argum>
Oct 19 22:28:46 web1.server2.tp3 systemd[1]: Started The nginx HTTP and reverse proxy server.
lines 1-18/18 (END)
 [...]
[kalop@web1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[kalop@web1 ~]$ sudo firewall-cmd --reload
success
```

- Curl depuis Marcel : 

```

[kalop@marcel ~]$ curl web1:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        background-color: #fff;
        color: #000;
        font-size: 0.9em;
        font-family: sans-serif, helvetica;
        margin: 0;
        padding: 0;
      }
      :link {
        color: #c00;
```


### 2. Partage de fichiers
#### B. Le setup wola

- Setup d'une nouvelle machine, qui sera un serveur NFS
```
[kalop@nfs1 ~]$ sudo dnf -y install nfs-utils
[...]
[kalop@nfs1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
[kalop@nfs1 ~]$ cat /etc/exports
/srv/nfs_share/ 10.3.0.195/28(rw,no_root_squash)
```

- Configuration du client NFS
```
[kalop@web1 ~]$ sudo dnf -y install nfs-utils
[...]
[kalop@web1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
[kalop@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share/ /srv/nfs/
[kalop@web1 ~]$ cat /etc/fstab
[...]
nfs1.server2.tp3:/srv/nfs_share/ /srv/nfs/           nfs     defaults        0 0
```

- tester que vous pouvez lire et écrire dans le dossier /srv/nfs depuis web1.server2.tp3
```
[kalop@web1 ~]$ cd /srv/nfs/
[kalop@web1 nfs]$ echo "test42" > test
[kalop@web1 nfs]$ ls
test
[kalop@web1 nfs]$ cat test
test42
```

- vous devriez voir les modifications du côté de  nfs1.server2.tp3 dans le dossier /srv/nfs_share/
```
[kalop@nfs1 ~]$ cd /srv/nfs_share/
[kalop@nfs1 nfs_share]$ ls
test
[kalop@nfs1 nfs_share]$ cat test
test42
```


## IV. Un peu de théorie : TCP et UDP
Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP : 

SSH : TCP
HTTP : TCP
DNS : UDP
NFS : TCP

## V. El final


![](https://i.imgur.com/Mn9mzSM.jpg)


