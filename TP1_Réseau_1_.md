# TP1 Réseau
## I. Exploration locale en solo
### 1. Affichage d'informations sur la pile TCP/IP locale
#### Affichez les infos des cartes réseau de votre PC:
![](https://i.imgur.com/PSOBwV8.png)
![](https://i.imgur.com/tH7fGaa.png)
- Comme on peut le voir sur les screen on obtient l nom l'adresse MAC et IP avec la commande ipconfig /all
#### Affichez votre gateway
- On peut voir sur le screen précedent la default gateway de la carte wifi qui corresspond a 10.33.3.253
#### En GUI Trouvez comment afficher les informations sur une carte IP 
![](https://i.imgur.com/NsCyZDD.png)
- Pour trouver les informations je suis allé danssettings , Network connections j'ai cliqué sur la carte réseaux concerné puis sur details ce qui me permer devoir l'adresse ip mac et la gateway 
#### à quoi sert la gateway dans le réseau d'YNOV ?
- La gateway sert a communiquer avec internet 
### 2. Modifications des informations
#### Utilisez l'interface graphique de votre OS pour changer d'adresse IP
![](https://i.imgur.com/JI3xX7l.png)
#### Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.
- J'ai perdu l'accès a internet car j'ai dus prendre la meme ip qu'une autre machine sur le réseau je pouvait donc envoyer des paquets mais pas en recevoir ce qui m'enpechecait d'acceder à interne
#### Exploration de la table ARP
![](https://i.imgur.com/JxS0r0w.png)
- j'ai utiliser arp -a pour afficher la table arp l'adresse mac de la gateway est 00-12-00-40-4c-bf pour la recupere j'ai regarder dans la table arp a quelle mac correspondait la mac de la gateway
####  Et si on remplissait un peu la table ?
![](https://i.imgur.com/5LR6kUj.png)
#### Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre
![](https://i.imgur.com/6pj3qG7.png)
#### Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap
![](https://i.imgur.com/58Nv4Ae.png)

![](https://i.imgur.com/POyj6fZ.png)
![](https://i.imgur.com/P1sconH.png)
## II. Exploration locale en duo


Owkay. Vous savez à ce stade :

afficher les informations IP de votre machine
modifier les informations IP de votre machine
c'est un premier pas vers la maîtrise de votre outil de travail

On va maintenant répéter un peu ces opérations, mais en créant un réseau local de toutes pièces : entre deux PCs connectés avec un câble RJ45.

- 1. Prérequis

deux PCs avec ports RJ45
un câble RJ45

firewalls désactivés sur les deux PCs


- 2. Câblage
Ok c'est la partie tendue. Prenez un câble. Branchez-le des deux côtés. Bap.


 ## Modification d'adresse IP
 Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

choisissez une IP qui commence par "192.168"
utilisez un /30 (que deux IP disponibles)

![](https://i.imgur.com/xKU4uO8.png)


- vérifiez à l'aide de commandes que vos changements ont pris effet

Nous voyons bien ici que notre adresse IP est comme nous voulons donc 192.168.10.2
 
 ```
 PS C:\Users\Muralee> ipconfig /all

 
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 3C-7C-3F-1A-6C-49
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::75ca:b818:94f4:af90%25(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.10.2(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
  
  
 ```

- utilisez ping pour tester la connectivité entre les deux machines
 
 
 La connexion est établi entre les deux ordinateur.
 ``` 
 PS C:\Users\Muralee> ping 192.168.10.1

Envoi d’une requête 'Ping'  192.168.10.1 avec 32 octets de données :
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.10.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
    
```
 
- Affichez et consultez votre table ARP

Nous trouvons ici la commande qui permets d'acceder à notre table ARP puis les informations concernants notre carte reseau Ethernet.

```
PS C:\Users\Muralee> arp -a

Interface : 192.168.10.2 --- 0x19
  Adresse Internet      Adresse physique      Type
  192.168.10.1          b4-a9-fc-9f-9b-46     dynamique
  192.168.10.3          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

```


## 4. Utilisation d'un des deux comme gateway
 
- pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu 

```
PS C:\Users\Muralee> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 20ms, Maximum = 22ms, Moyenne = 21ms
 

```



- utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```
PS C:\Users\Muralee> tracert 192.168.10.1

Détermination de l’itinéraire vers LAPTOP-FLEOPDHT [192.168.10.1]
avec un maximum de 30 sauts :

  1     1 ms     1 ms     1 ms  LAPTOP-FLEOPDHT [192.168.10.1]

Itinéraire déterminé.
PS C:\Users\Muralee> tracert 1.1.1.1

Détermination de l’itinéraire vers 1.1.1.1 avec un maximum de 30 sauts.

  1     1 ms     *        2 ms  LAPTOP-FLEOPDHT [192.168.10.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     8 ms    12 ms     5 ms  10.33.3.253
  4     8 ms     5 ms     4 ms  10.33.10.254
  5    14 ms    10 ms    10 ms  92.103.174.137
  6    12 ms    11 ms    11 ms  92.103.120.182
  7    24 ms    22 ms    21 ms  172.19.130.117
  8    25 ms    24 ms    24 ms  46.218.128.74
  9    48 ms    22 ms    22 ms  195.42.144.143
 10    22 ms    19 ms    19 ms  1.1.1.1

Itinéraire déterminé.

```

## 5. Petit chat privé

On va créer un chat extrêmement simpliste à l'aide de netcat (abrégé nc). Il est souvent considéré comme un bon couteau-suisse quand il s'agit de faire des choses avec le réseau.


L'idée ici est la suivante :

l'un de vous jouera le rôle d'un serveur

l'autre sera le client qui se connecte au serveur




sur le PC serveur avec par exemple l'IP 192.168.1.1

nc.exe -l -p 8888

"netcat, écoute sur le port numéro 8888 stp"

il se passe rien ? Normal, faut attendre qu'un client se connecte



- sur le PC client avec par exemple l'IP 192.168.1.2


nc.exe 192.168.1.1 8888

"netcat, connecte toi au port 8888 de la machine 192.168.1.1 stp"


une fois fait, vous pouvez taper des messages dans les deux sens

![](https://i.imgur.com/4gUtS0Y.png)



## 6. Firewall


- Activez votre firewall
    
    ![](https://i.imgur.com/CtUGFhB.png)



#### Autoriser les ping

- Configurer le firewall de votre OS pour accepter le ping

Pour ce faire, il faut d'abord lancer son powershell ou cmd en tant qu'administrateur puis effectuer la commande 
suivante ( nous avons aussi d'autres manieres d'effectuer ceci mais j'ai trouvé que c'était plus simple de passer par notre cmd)

```
PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.

```



- Autoriser le traffic sur le port qu'utilise nc

- Ouverture de port TCP et/ou UDP



choisissez arbitrairement un port entre 1024 et 20000
vous utiliserez ce port pour communiquer avec netcat par groupe de 2 toujours
le firewall du PC serveur devra avoir un firewall activé et un netcat qui fonctionne



![](https://i.imgur.com/MrspMPQ.png)


```
Apres avoir activer et ouvert un port TCP, nous avons réalisé de nouveau le chat avec netcat.

C:\Users\Muralee\Desktop\netcat-1.11>nc.exe 192.168.10.1 8888

```


Voici le resultat qu'on obtient apres nos modifications.
(toujours la même chose :) )

![](https://i.imgur.com/Y8muXAc.png)



## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP
Bon ok vous savez définir des IPs à la main. Mais pour être dans le réseau YNOV, vous l'avez jamais fait.
C'est le serveur DHCP d'YNOV qui vous a donné une IP.
Une fois que le serveur DHCP vous a donné une IP, vous enregistrer un fichier appelé bail DHCP qui contient, entre autres :

l'IP qu'on vous a donné
le réseau dans lequel cette IP est valable

-Exploration du DHCP, depuis votre PC

afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
cette adresse a une durée de vie limitée. C'est le principe du bail DHCP (ou DHCP lease). Trouver la date d'expiration de votre bail DHCP
vous pouvez vous renseigner un peu sur le fonctionnement de DHCP dans les grandes lignes. On aura sûrement un cours là dessus :)

``` 

C:\Cours_B2\Linux\netcat>ipconfig /all

{...}
Carte réseau sans fil Wi-Fi :

   {...}
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   {...}
   Bail obtenu. . . . . . . . . . . . . . : vendredi 17 septembre 2021 13:43:20
   Bail expirant. . . . . . . . . . . . . : samedi 18 septembre 2021 13:43:34
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   {...}


``` 


### 2. DNS

-  trouver l'adresse IP du serveur DNS que connaît votre ordinateur

- utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

![](https://i.imgur.com/QDOQpqt.png)

![](https://i.imgur.com/KnL2QHd.png)

- On peut voir que l'adresse du serveur dns que mon pc utilisie est fe80::a63e:51ff:fe68:2a46
- nslookup permet de voir l'adresse ip associé a un nom de domaine 92.243.16.143 pour ynov.com par exemple
- faites un reverse lookup
 pour l'adresse 78.74.21.21
 pour l'adresse 92.146.54.88

![](https://i.imgur.com/g83U4R8.png)

- on peut utliser la command nslookup pour faire une reverse lookup qui nous donnera un nom de domain a partir d'une ip comme on peut le voir dans l'output de la commande 


## IV. Wireshark
Wireshark est un outil qui permet de visualiser toutes les trames qui sortent et entrent d'une carte réseau.
Il peut :

enregistrer le trafic réseau, pour l'analyser plus tard
afficher le trafic réseau en temps réel

On peut TOUT voir.
Un peu austère aux premiers abords, une manipulation très basique permet d'avoir une très bonne compréhension de ce qu'il se passe réellement.

téléchargez l'outil Wireshark


- utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

un ping entre vous et la passerelle
un netcat entre vous et votre mate, branché en RJ45
une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
prenez moi des screens des trames en question
on va prendre l'habitude d'utiliser Wireshark souvent dans les cours, pour visualiser ce qu'il se passe


###### Ping 

![](https://i.imgur.com/PTcBATU.png)



###### Netcat 

![](https://i.imgur.com/wQFQsf3.png)




###### DNS 

![](https://i.imgur.com/PaysIgM.png)
