# TP2 Réseau
## I. ARP
### 1. Echange ARP
#### Générer des requêtes ARP
- effectuer un ping d'une machine à l'autre
```
[kalop@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.839 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.969 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.416 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=1.02 ms
64 bytes from 10.2.1.12: icmp_seq=5 ttl=64 time=0.437 ms
64 bytes from 10.2.1.12: icmp_seq=6 ttl=64 time=0.497 ms
64 bytes from 10.2.1.12: icmp_seq=7 ttl=64 time=0.773 ms
64 bytes from 10.2.1.12: icmp_seq=8 ttl=64 time=0.348 ms
^C
--- 10.2.1.12 ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7178ms
rtt min/avg/max/mdev = 0.348/0.661/1.016/0.252 ms
```
- observer les tables ARP des deux machines
```
[kalop@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:5c [ether] on enp0s8
? (10.2.1.12) at 08:00:27:51:0c:1f [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
```
```
[kalop@node2 ~]$ arp -a
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.11) at 08:00:27:ae:51:b9 [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:5c [ether] on enp0s8
```
- repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa

  on peut voir que l'adresse MAC de node1 est 08:00:27:ae:51:b9 et celle de node2 est 08:00:27:51:0c:1f

- une commande pour voir la table ARP de node1
```
[kalop@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:5c [ether] on enp0s8
? (10.2.1.12) at 08:00:27:51:0c:1f [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
```

- une commande pour voir la MAC de node2
ip a
```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:51:0c:1f brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe51:c1f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
### 2. Analyse de trames
#### Analyse de trames
- utilisez la commande tcpdump pour réaliser une capture de trame
```
[kalop@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_arp.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C19 packets captured
20 packets received by filter
0 packets dropped by kernel
```
- videz vos tables ARP, sur les deux machines, puis effectuez un ping

```
[kalop@node2 ~]$ sudo ip -s -s neigh flush all
[sudo] password for kalop:
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 ref 1 used 8/3/3 probes 1 REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:ae:51:b9 used 1071/1071/1043 probes 1 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:5c ref 1 used 10/0/8 probes 4 REACHABLE

*** Round 1, deleting 3 entries ***
*** Flush is complete after 1 round ***
```

```
[kalop@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.643 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.209 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.310 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.159 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3063ms
rtt min/avg/max/mdev = 0.159/0.330/0.643/0.189 ms
```

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP |`node1``08:00:27:ae:51:b9` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `08:00:27:51:0c:1f` | `node1` `08:00:27:ae:51:b9`   |
| 3   | Requête ARP       | `08:00:27:51:0c:1f`                 | `08:00:27:ae:51:b9`  |
| 4   | Réponse ARP       | `08:00:27:ae:51:b9`                 | `08:00:27:51:0c:1f`  |


## II. Routage
### 1. Mise en place du routage
- Activer le routage sur le noeud router.2.tp2
```
[kalop@router ~]$ [kalop@router ~]$ sudo nano /proc/sys/net/ipv4/ip_forward
[sudo] password for kalop:
[kalop@router ~]$ [kalop@router ~]$ cat /proc/sys/net/ipv4/ip_forward
1
[kalop@router ~]$
```
- Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping
```
[kalop@node1 ~]$ ip route show
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
```
```
[kalop@marcel ~]$ ip route show
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

```
[kalop@node1 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.427 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=1.10 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=1.10 ms
^C
--- 10.2.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2021ms
rtt min/avg/max/mdev = 0.427/0.873/1.097/0.316 ms
[kalop@node1 ~]$

```

```
[kalop@marcel ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=63 time=1.76 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=63 time=1.27 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=63 time=2.05 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=63 time=2.29 ms
64 bytes from 10.2.1.12: icmp_seq=5 ttl=63 time=2.45 ms
^C
--- 10.2.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 1.272/1.962/2.447/0.416 ms
```

### 2. Analyse de trames
- Analyse des échanges ARP

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `08:00:27:ae:51:b9`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `routeur` `08:00:27:ef:aa:95` | x              | `node1` `08:00:27:ae:51:b9`   |
| 3   | Requête IMCP         | `10.2.1.12`      | `node1` `08:00:27:ae:51:b9`  |  `10.2.2.12`              | `routeur`  `08:00:27:ef:aa:95`                         |
| 4     | Requête ARP        | x        | `routeur` `08:00:27:23:b4:76`                         | x              | Broadcast `ff:ff:ff:ff:ff:ff`                          |
| 5     | Réponse ARP        | x        | `marcel` `08:00:27:02:c9:15`                         | x              | `routeur` `08:00:27:23:b4:76`                         |
|6 | Requête IMCP | `10.2.2.254` | `routeur` `08:00:27:23:b4:76` | `10.2.2.12` | `marcel` `08:00:27:02:c9:15` |
|7 | Réponse IMCP | `10.2.2.12` | `marcel` `08:00:27:02:c9:15` | `10.2.2.254`| `routeur` `08:00:27:23:b4:76`|
|8 | Réponse IMCP | `10.2.2.12` | `routeur` `08:00:27:ef:aa:95` | `10.2.1.12` | `node1` `08:00:27:ae:51:b9` |

### 3. Accès internet
- Donnez un accès internet à vos machines
```
[kalop@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8s/ifcfg-enp0s8
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.2.1.12
NETMASK=255.255.255.0
GATEWAY=10.2.1.254
DNS1=1.1.1.1
[kalop@node1 ~]$ ping google.com
PING google.com (142.250.75.238) 56(84) bytes of data.
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=114 time=27.6 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=114 time=28.4 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 27.640/27.999/28.359/0.396 ms
[kalop@node1 ~]$
```
```
[kalop@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27332
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             277     IN      A       142.250.75.238

;; Query time: 27 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 18:30:10 CEST 2021
;; MSG SIZE  rcvd: 55

[kalop@node1 ~]$
```

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |     |
|-------|------------|---------------------|--------------------------|----------------|-----------------|-----|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:ae:51:b9` | `8.8.8.8`      | `routeur` `08:00:27:ef:aa:95`               |     |
| 2     | pong       |`8.8.8.8`                 |  `routeur` `08:00:27:ef:aa:95` | `node1` `10.2.1.12` |   `node1` `08:00:27:ae:51:b9`   | 

## III. DHCP
### 1. Mise en place du serveur DHCP

- Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP
```
[kalop@node1 dhcp]$ sudo cat dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.10 10.2.1.200;
  option routers 10.2.1.254;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 1.1.1.1;

}
[kalop@node1 dhcp]$
```
- Améliorer la configuration du DHCP

```
[kalop@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
NAME=enp0s8
UUID=b5c8e65c-5c7a-4a99-a568-2b8dcbe1a697
DEVICE=enp0s8
ONBOOT=yes
NETMASK=255.255.255.0
[kalop@node2 ~]$
```

```
[kalop@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:17:44 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:51:0c:1f brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.10/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 833sec preferred_lft 833sec
    inet6 fe80::a00:27ff:fe51:c1f/64 scope link
       valid_lft forever preferred_lft forever
[kalop@node2 ~]$
```

```
[kalop@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.260 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.381 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=0.235 ms
^C
--- 10.2.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2048ms
rtt min/avg/max/mdev = 0.235/0.292/0.381/0.063 ms
[kalop@node2 ~]$
```

```
[kalop@node2 ~]$ ip route show
default via 10.2.1.254 dev enp0s8 proto dhcp metric 101
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.10 metric 101
[kalop@node2 ~]$
```

```
[kalop@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=24.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=24.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=24.7 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 24.701/24.821/24.897/0.201 ms
[kalop@node2 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 51469
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             66      IN      A       172.217.19.238

;; Query time: 29 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 19:53:13 CEST 2021
;; MSG SIZE  rcvd: 55

[kalop@node2 ~]$ ping google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=1 ttl=113 time=27.6 ms
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=2 ttl=113 time=25.1 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 25.096/26.370/27.645/1.284 ms
[kalop@node2 ~]$
```
### 2. Analyse de trames
- Analyse de trames




