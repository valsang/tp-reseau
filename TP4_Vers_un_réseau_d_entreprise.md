# TP4 : Vers un réseau d'entreprise
## I. Dumb switch
### 1. Topologie 1
### 2. Adressage topologie 1
### 3. Setup topologie 1
- définissez les IPs statiques sur les deux VPCS
```
PC1> ip 10.1.1.1/24
PC2> ip 10.1.1.2/24
```
- ping un VPCS depuis l'autre
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=20.718 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=26.165 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=20.725 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=21.917 ms
^C
PC2>
```

## II. VLAN
### 3. Setup topologie 2
- définissez les IPs statiques sur tous les VPCS
```
PC3> ip 10.1.1.3/24
```
- vérifiez avec des ping que tout le monde se ping
```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=29.035 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=36.047 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=19.868 ms
^C
PC3>
```
```
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=19.115 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=22.991 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=39.582 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=15.173 ms
^C
```
- Configuration des VLANs

```
(config)# vlan 10
(config-vlan)# name 1
(config-vlan)# exit

(config)# vlan 20
(config-vlan)# name 2
(config-vlan)# exit
```

```
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
```

```
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   1                                active    Gi0/0, Gi0/1
20   2                                active    Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```
-  Vérif
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=5.959 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=11.534 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=14.074 ms
^C
PC2>
```
```
PC3> ping 10.1.1.1

^Chost (10.1.1.1) not reachable

PC3>
```
## III. Routing
- Configuration des VLANs
```

Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
11   11                               active    Gi0/0, Gi0/1
12   12                               active    Gi0/2
13   13                               active    Gi0/3
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup

```

```
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       1,11-13
Switch#
```
- Config du routeur
```
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
FastEthernet1/0            unassigned      YES unset  administratively down down
FastEthernet2/0            unassigned      YES unset  administratively down down
FastEthernet3/0            unassigned      YES unset  administratively down down
```
- Vérif
- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
```
pc1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=20.028 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=10.939 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=17.427 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=18.117 ms
^C
pc1>
```
- testez des ping entre les réseau
```
pc1> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=25.816 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=38.694 ms
84 bytes from 10.3.3.1 icmp_seq=3 ttl=63 time=22.011 ms
^C
pc1>
```

## IV. NAT
### 3. Setup topologie 4
- côté routeur, il faudra récupérer un IP en DHCP
```
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
FastEthernet1/0            192.168.180.5   YES DHCP   up                    up
FastEthernet2/0            unassigned      YES unset  administratively down down
FastEthernet3/0            unassigned      YES unset  administratively down down
```
